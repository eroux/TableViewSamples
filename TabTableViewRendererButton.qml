import QtQuick 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4

Item {
   id: tableViewRendersItem
   width: 700
   height: 500

   //   Component.onCompleted: {
   //      tableView.resizeColumnsToContents()
   //   }
   Item {
      id: item1
      anchors.fill: parent
      TableView {
         id: tableView
         frameVisible: false
         alternatingRowColors: true
         model: sourceModel

         readonly property string surveyOn: "Survey ON"
         readonly property string surveyOff: "Survey OFF"
         readonly property string interlockOk: "NORMAL"
         readonly property string interlockBad: "ERROR"

         height: item1.height - refreshButton.height - 6
         horizontalScrollBarPolicy: 0
         headerVisible: true
         selectionMode: 0
         sortIndicatorColumn: 1
         // https://bugreports.qt.io/browse/QTBUG-54355
         sortIndicatorVisible: true
         sortIndicatorOrder: Qt.AscendingOrder
         anchors.right: parent.right
         anchors.rightMargin: 0
         anchors.left: parent.left
         anchors.leftMargin: 0

         TableViewColumn {
            id: userColumn
            title: "TGM User name"
            role: "user"
            movable: false
            resizable: false
            horizontalAlignment: Text.AlignHCenter
            width: tableView.viewport.width * 0.4
            delegate: userDelegate
         }

         TableViewColumn {
            id: surveyStatusColumn
            title: "Survey status"
            role: "surveyStatus"
            movable: false
            resizable: false
            horizontalAlignment: Text.AlignHCenter
            width: tableView.viewport.width * 0.2
            delegate: surveyDelegate
         }

         TableViewColumn {
            id: blmResetColumn
            title: "Blm Reset PPM"
            role: "blmReset"
            movable: false
            resizable: false
            horizontalAlignment: Text.AlignHCenter
            width: tableView.viewport.width * 0.2
            delegate: resetDelegate
         }

         TableViewColumn {
            id: interlockColumn
            title: "Interlock status HW"
            role: "interlockStatus"
            movable: false
            resizable: false
            horizontalAlignment: Text.AlignHCenter
            width: tableView.viewport.width * 0.2
            delegate: interlockDelegate
         }

         Component {
            id: userDelegate
            Text {
               anchors.verticalCenter: parent.verticalCenter
               anchors.fill: parent
               elide: styleData.elideMode
               text: styleData.value
               color: styleData.textColor
            }
         }

         Component {
            id: surveyDelegate
            Text {
               anchors.verticalCenter: parent.verticalCenter
               anchors.fill: parent
               anchors.centerIn: parent
               elide: styleData.elideMode
               text: styleData.value
               color: styleData.value === tableView.surveyOn ? "green" : "red"
               horizontalAlignment: Text.AlignHCenter
            }
         }
         Component {
            id: resetDelegate
            Button {
               text: "Reset"
               anchors.verticalCenter: parent.verticalCenter
               anchors.fill: parent
               onClicked: {
                  console.log(
                           "Resetting BE.BLM/Reset#reset on " + sourceModel.get(
                              styleData.row).user + " @Row=" + styleData.row)
                  // TODO change cell color according to received value
               }
            }
         }
         Component {
            id: interlockDelegate
            Text {
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenter: parent.verticalCenter
               anchors.fill: parent
               anchors.centerIn: parent
               elide: styleData.elideMode
               text: styleData.value
               color: styleData.value === tableView.interlockOk ? "green" : "red"
               horizontalAlignment: Text.AlignHCenter
            }
         }

         ListModel {
            id: sourceModel
            property variant plsArray: ["PSB.USER.AD", "PSB.USER.EAST1", "PSB.USER.EAST2", "PSB.USER.LHC1A", "PSB.USER.LHC1B", "PSB.USER.LHC2A", "PSB.USER.LHC2B", "PSB.USER.LHC3", "PSB.USER.LHC4", "PSB.USER.LHCINDIV", "PSB.USER.LHCPROBE", "PSB.USER.MD1", "PSB.USER.MD2", "PSB.USER.MD3", "PSB.USER.MD4", "PSB.USER.MD5", "PSB.USER.MD6", "PSB.USER.NORMGPS", "PSB.USER.NORMHRS", "PSB.USER.SFTPRO1", "PSB.USER.SFTPRO2", "PSB.USER.STAGISO", "PSB.USER.TOF", "PSB.USER.ZERO"]
            Component.onCompleted: {
               //console.log("plsArray.count=" + plsArray.length)
               for (var row = 0; row < plsArray.length; row++) {
                  append({
                            "user": plsArray[row],
                            "surveyStatus": row % 2 == 0 ? tableView.surveyOn : tableView.surveyOff,
                                                           "interlockStatus": row % 5 == 0 ? tableView.interlockOk : tableView.interlockBad
                         })
               }
            }
         }
      }

      Button {
         id: refreshButton
         height: 30
         text: "refresh"
         anchors.right: parent.right
         anchors.rightMargin: 5
         anchors.left: parent.left
         anchors.leftMargin: 5
         anchors.top: tableView.bottom
         anchors.topMargin: 3
         // anchors.fill.width: parent.width
         onClicked: {
            for (var row = 0; row < sourceModel.count; row++) {
               console.log("Doing GET on BE.BLM/Acquisition#chanStatus [" + row
                           + "]= " + sourceModel.get(row).user)
               console.log("Doing GET on BE.BLM/InterlockData#interlockStatus ["
                           + row + "]= " + sourceModel.get(row).user)
            }
         }
      }
   }
}
