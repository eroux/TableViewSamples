import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4

ApplicationWindow {
   id: applicationWindow
   title: qsTr("Qt TableView examples")
   visible: true
   width: 850
   height: 500

   TabPane {
      id: tabPane
      // select third tab
      //currentIndex: 3
      currentIndex: 5
   }
}
