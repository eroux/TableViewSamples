import QtQuick 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls 2.4 as QT2

Rectangle {
   id: rectangle
   ListModel {
      id: tableModel
   }

   // http://doc.qt.io/qt-5/qml-qtquick-keys.html
   Keys.onPressed: {
      switch (event.key) {
      case Qt.Key_Shift:
         keyPressed.text = "Shift"
         break
      case Qt.Key_Control:
         keyPressed.text = "Control"
         break
      case Qt.Key_Meta:
         keyPressed.text = "Meta"
         break
      case Qt.Key_Alt:
         keyPressed.text = "Alt"
         break
      case Qt.Key_AltGr:
         keyPressed.text = "Altgr"
         break
      default:
         keyPressed.text = "N/A"
         break
      }
      event.accepted = false
   }

   Keys.onReleased: {
      // update keyTitle label when above META keys are not pressed
      keyPressed.text = "N/A"
   }

   ToolBar {
      id: toolBar
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0

      Text {
         id: title
         text: qsTr("Selection mode (> 0) ")
         font.weight: Font.Bold
         anchors.verticalCenter: parent.verticalCenter
         font.pixelSize: 13
      }
      Text {
         id: selectionModeLabel
         text: tableView.selectionMode
         font.weight: Font.Bold
         color: "red"
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: title.right
         anchors.leftMargin: 3
         font.pixelSize: 13
      }
      TextField {
         id: selectionModeTextField
         width: 40
         text: tableView.selectionMode
         anchors.left: selectionModeLabel.right
         anchors.leftMargin: 3
         anchors.verticalCenter: parent.verticalCenter
         onAccepted: {
            tableView.selectionMode = text
         }
      }

      Button {
         id: clearSelectionBtn
         text: "Clear selection"
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: selectionModeTextField.right
         anchors.leftMargin: 3
         onClicked: tableView.selection.clear()
      }

      Button {
         id: selectAllBtn
         text: "Select ALL"
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: clearSelectionBtn.right
         anchors.leftMargin: 3
         onClicked: tableView.selection.selectAll()
      }
      Text {
         id: keyTitle
         text: "Key pressed:"
         font.weight: Font.Bold
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: selectAllBtn.right
         anchors.leftMargin: 3
         font.pixelSize: 13
      }
      Text {
         id: keyPressed
         color: "blue"
         text: "N/A"
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: keyTitle.right
         anchors.leftMargin: 3
         font.pixelSize: 13
      }
   }

   TableView {
      id: tableView
      anchors.top: toolBar.bottom
      anchors.right: parent.right
      anchors.left: parent.left
      anchors.topMargin: 0
      frameVisible: false

      // data sorting is not implemented by default (Not working)
      // https://bugreports.qt.io/browse/QTBUG-54355
      sortIndicatorVisible: true
      sortIndicatorOrder: Qt.AscendingOrder
      sortIndicatorColumn: 1

      alternatingRowColors: true
      // uncomment the following line to CONTROL the alternate row (characteristics: color, heigths, etc) color value
      // as it is gray by default with the above statement
      rowDelegate: Component {
         id: rowDelegate
         // allow rows of a specific height
         Rectangle {
            width: tableView.width
            // can be done also using  alternatingRowColors: true on TableView
            // in this case we must handle ourself the selection color otherwise the row is blank with blank text
            color: styleData.selected ? "#81BEF7" : (styleData.row % 2 == 0 ? "#ECCEF5" : "white")
            height: 20
         }
      }

      // global delegate / renderer for ALL columns cells
      itemDelegate: Component {
         id: itemDelegate

         Text {
            // to align the cell
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: styleData.textColor
            text: styleData.value
            font.bold: styleData.column % 2 == 0

            ///////////////////////////////////////
            // Cell Popup menu handling START
            MouseArea {
               id: mouseArea
               anchors.fill: parent
               acceptedButtons: Qt.RightButton
               propagateComposedEvents: true

               onClicked: {
                  //  console.log(
                  //                        "btn=" + mouse.button + " isRigthButton="
                  //                        + (mouse.button === Qt.RightButton) + " mods=" + mouse.modifiers)
                  // @BUG: the popup menu is shown when presing the CTRL key and left clicking
                  // so even if I check RightButton only, I have to check the modifiers too
                  if (mouse.button === Qt.RightButton && mouse.modifiers === 0)
                     contextMenu.popup()
                  mouse.accepted = false
               }
            }
            // https://doc.qt.io/qt-5.11/qml-qtquick-controls2-menu.html
            QT2.Menu {
               id: contextMenu
               width: titleLabel.width - 2
               QT2.Label {
                  id: titleLabel
                  text: "  Contextual Menu example  "
                  horizontalAlignment: Text.AlignHCenter
                  verticalAlignment: Text.AlignVCenter
                  style: Text.Raised
                  color: "#AAAAAA"
                  font.bold: true
                  font.pointSize: 11
                  height: 30
                  background: Rectangle {
                     color: "#EEEEEE"
                     width: titleLabel.width - 4
                     height: titleLabel.height - 2
                     x: 1
                     y: 1
                  }
               }

               QT2.MenuItem {
                  text: " styleData.selected=[" + styleData.selected + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.value=[" + styleData.value + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.textColor=[" + styleData.textColor + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.row=[" + styleData.row + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.column=[" + styleData.column + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.elideMode=[" + styleData.elideMode + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.textAlignment=[" + styleData.textAlignment + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.pressed=[" + styleData.pressed + "]"
                  height: 20
               }
               QT2.MenuItem {
                  text: " styleData.hasActiveFocus=[" + styleData.hasActiveFocus + "]"
                  height: 20
               }
            }
            // Cell Popup menu handling END
            ///////////////////////////////////////
         }
      }

      ///////////////////////////////////////
      // TableView Column declaration START
      TableViewColumn {
         title: "Name"
         role: "name"
      }
      TableViewColumn {
         title: "IP Address"
         role: "ipAddress"
         width: 240
      }
      TableViewColumn {
         title: "Status"
         role: "status"
         width: 60
         horizontalAlignment: Text.AlignHCenter
         resizable: false
      }
      // TableView Column declaration END
      ///////////////////////////////////////

      //////////////////////////////////////
      // TableView Model handling START
      model: tableModel
      Component.onCompleted: {
         for (var i = 0; i < 10; i++)
            tableModel.append({
                                 "name": "Dummy name " + i,
                                 "ipAddress": "192.168.2." + i,
                                 "status": "Ok"
                              })
      }
      // TableView Model handling END
      //////////////////////////////////////

      ///////////////////////////////////////
      // TableView selection handling START
      // http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html#selection-prop
      selectionMode: 2
      // why this code is never called ?????
      // @BUG: https://bugreports.qt.io/browse/QTBUG-49787
      onSelectionChanged: {
         console.log("!SelectionChanged! Row=[" + row + "]")
         textArea.append("!SelectionChanged! Row=[" + row + "]")
      }
      // The proposed workaround in above bug report is working
      Connections {
         target: tableView.selection
         onSelectionChanged: {
            console.debug("onSelectionChanged:" + tableView.selection.count)
            console.debug("onSelectionChanged:" + tableView.selection)
            // how to retreive the selected row indexes ?
            console.debug("onSelectionChanged:" + tableView.selection.to)
         }
      }
      // TableView selection handling END
      ///////////////////////////////////////

      // for debuging event behaviors
      //      onSelectionModeChanged: {
      //         console.log("!SelectionModeChanged! SelectionMode:" + selectionMode)
      //      }
      //      onClicked: {
      //         console.log(
      //                  "!Clicked! Row:" + row + " currentRow:" + currentRow + " selection:" + selection)
      //      }
      //      onActivated: {
      //         console.log("!Activated! Row:" + row + " Col:" + column
      //                     + " currentRow:" + currentRow + " selection:" + selection)
      //      }
      //      onCurrentRowChanged: {
      //         console.log(
      //                  "!CurrentRow! currentRow:" + currentRow + " selection:" + selection)
      //      }
      //      onPressAndHold: {
      //         console.log("!PressAndHold! Row:" + row + " currentRow:" + currentRow
      //                     + " selection:" + selection)
      //      }
   }

   TextArea {
      id: textArea
      anchors.top: tableView.bottom
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.topMargin: 0

      readOnly: true
      //textFormat: Qt.RichText
      frameVisible: false
   }
}
