import QtQuick 2.11
import QtQuick.Controls 1.4

Rectangle {
   id: root
   width: parent ? parent.width : 500
   height: parent ? parent.height : 500
   color: "#333333"

   ListModel {
      id: live_alertmodel
   }

   TableView {
      anchors.fill: parent
      height: 100

      TableViewColumn {
         role: "time"
         title: "Time"
         width: root.width / 4
         delegate: textDelegate
      }

      TableViewColumn {
         role: "location"
         title: "Location"
         width: root.width / 4
         delegate: textDelegate
      }

      TableViewColumn {
         role: "alert"
         title: "Alert"
         width: root.width / 4
         delegate: textDelegate
      }

      TableViewColumn {
         role: "image"
         title: "Image"
         width: root.width / 4
         delegate: imageDelegate
      }

      model: live_alertmodel

      Component.onCompleted: {
         for (var i = 0; i < 10; i++)
            live_alertmodel.append({
                                      "time": "0" + i + "/15/2018",
                                      "location": "location",
                                      "alert": "access",
                                      "image": "images/globe.jpg"
                                   })
      }
   }

   // Text renderer
   Component {
      id: textDelegate
      Item {
         id: f_item
         height: cell_txt.height
         Text {
            id: cell_txt
            width: parent.width
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            text: styleData.value
            elide: Text.AlignHCenter
            renderType: Text.NativeRendering
         }
      }
   }

   // Image renderer
   Component {
      id: imageDelegate
      Item {
         Image {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
            height: 20
            cache: true
            asynchronous: true
            source: styleData.value
         }
      }
   }
}
