#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "sortfilterproxymodel.h"


int main(int argc, char *argv[])
{
    qmlRegisterType<SortFilterProxyModel>("org.qtproject.example", 1, 0, "SortFilterProxyModel");

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);


    QQmlApplicationEngine engine;

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

     if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
