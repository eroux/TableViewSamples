
:bangbang: This project has been MOVED to the [`qt-qml-evaluation` gitlab project](https://gitlab.cern.ch/acc-co/accsoft/gui/qt-evaluation/qt-cpp-integration/tree/master/qt-qml-evaluation) :bangbang:
---
<a name="TOC"></a>
**Table of contents**
- [TableView usage samples](#introduction)
   - [Application structure](#structure)
   - [TabTableViewScroll.qml](#TabTableViewScroll) - [src](TabTableViewScroll.qml)
   - [TabTableViewSort.qml](#TabTableViewSort) - [src](TabTableViewSort.qml)
   - [TabTableViewRendererButton.qml](#TabTableViewRendererButton) - [src](TabTableViewRendererButton.qml)
   - [TabTableViewRendererImage.qml](#TabTableViewRendererImage) - [src](TabTableViewRendererImage.qml)
   - [TabTableViewRowAndItemDelegate.qml](#TabTableViewRowAndItemDelegate) - [src](TabTableViewRowAndItemDelegate.qml)
   - [TabTableViewAddRemove.qml](#TabTableViewAddRemove) - [src](TabTableViewAddRemove.qml)


# TableView usage samples <a name="introduction"></a>
[|Up|](#TOC) This project contains several QML files examples in order to study and demo the QML [TableView](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html) capabilities such as the selection, contextual menu support, cell renderer customisation, etc.



## Application structure <a name="structure"></a>
[|Up|](#TOC) The [`main.qml`](main.qml) source file is containing a TabView (TabPane.qml) showing some sample usages of the [TableView](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html) in separates tab it is loaded at startup by the [`main.cpp`](main.cpp) file.(in red in the project tree screen dump below)
![Project tree](https://issues.cern.ch/secure/attachment/56747/ApplicationStructure.png)

The associated QML source files for each tab is following the `TabTableViewXxx.qml` naming convention, with Xxx being the name of the exposed TableView capability.(in blue in the above project tree screen dump)

The [`RoundedTab.qml`](RoundedTab.qml) and [`RoundedTabForm.ui.qml`](RoundedTabForm.ui.qml) source files are the specific implementation for my custom  [`TabPane.qml`](TabPane.qml) and [`TabPane.ui.qml`](TabPane.ui.qml) [TabView](http://doc.qt.io/qt-5/qml-qtquick-controls-tabview.html) implementation as the default implemenattion is too *flat* to me.(in green in the above project tree screen dump)

:confused: The  [`cities.json`](cities.json) file is not yet used as I could not managed to read this file from QML without using C++ call yet.

Notice that you may found more details about the implementation of these TableView examples in the [GUI-105](https://issues.cern.ch/browse/GUI-105) Jira issue.



##  TabTableViewScroll.qml <a name="TabTableViewScroll"></a>
[|Up|](#TOC) This [source file](TabTableViewScroll.qml) is containing the QML *emulation* of [the Java CPS vistar] (https://op-webtools.web.cern.ch/Vistar/vistars.php?usr=CPS) cycle scrolling table widget displayed in the lower left corner and showing the currently played (and the next cycle at the bottom of the scrolling table) CPS cycle information: Basic period number, LSAcycle name, TGM cycle index, intensity, particle type and destination.

The 10 previously played cycles in the history are stored in the scrolling table above the white line separator.
When a new supercycle is started a red line separator is inserted in the table too.
If you need more details about the TGM cycles, supercyckles and the BE-CO timing system in general please check [this page](https://wikis.cern.ch/display/TIMING/Central+Timing).

:bangbang: **IMPORTANT:** I am displaying **fake data** for the moment and the QML code will need to be adapted to display real device's data as soon as they will be available.

Here is the original CPS Vistar scrolling table implementation displaying real data:
![Java original  implementation](https://issues.cern.ch/secure/attachment/56729/CPSVistar.gif)

Here is the QML equivalent implementation displaying FAKE data:
![QML implementation](https://issues.cern.ch/secure/attachment/56733/CPSVistar2.gif)



## TabTableViewSort.qml <a name="TabTableViewSort"></a>
[|Up|](#TOC) This [source file](TabTableViewSort.qml) is containing a QML example of sorting and filtering [TableView](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html) column data. 
It was blindly copied from the [Qt documentation](http://doc.qt.io/qt-5/qtquickcontrols-tableview-example.html).
It needs the `sortfilterproxymodel.cpp` class to do the sorting and filtering of column's data.

![Table sorting and filtering](https://issues.cern.ch/secure/attachment/56246/TableViewSample1.gif)


## TabTableViewRendererButton.qml <a name="TabTableViewRendererButton"></a>
[|Up|](#TOC) This [source file](TabTableViewRendererButton.qml)  is containing a QML example using a [Button](http://doc.qt.io/qt-5/qml-qtquick-controls-button.html) component as [TableView](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html) cell renderer / Item delegate.

![QButton renderer](https://issues.cern.ch/secure/attachment/56745/ButtonRenderer1.png)



## TabTableViewRendererImage.qml <a name="TabTableViewRendererImage"></a>
[|Up|](#TOC) This [source file](TabTableViewRendererImage.qml) is containing a QML example using an [Image](http://doc.qt.io/qt-5/qml-qtquick-image.html) component as [TableView](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html) cell renderer / Item delegate.

![Image renderer](https://issues.cern.ch/secure/attachment/56743/ImageRenderer.png)



## TabTableViewRowAndItemDelegate.qml <a name="TabTableViewRowAndItemDelegate"></a>
[|Up|](#TOC) This [source file](TabTableViewRowAndItemDelegate.qml) is containing a QML example to illustrate the Contextual menu and row selection support in a [TableView](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html)

Right clicking anyy cell will display a contextual menu showing information about the selected TableView cell data. The object cobntaining all the data associated with the seklected cell is called the [`styleData`](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html#itemDelegate-prop).

I added also a textfield in the toolbar allowing you to change the TableView `selectionMode` using a number beween 0 to 4:
- 0: no selection
- 1: single row selection
- 2: multiple row selection (use ctrl to select multiple row, any click without CTRL deselect all)
- 3: multiple row selection (click to select/deselect any row)
- >=4: multiple row selection (click and drag to select/deselect any consecutive rows any click deselect all and select the clicked row)

I have added a label (blue text) to show if a META key (Shift, Ctrl, Alt,etc) is pressed during row selection.

:confused: I could not manage **yet** to found how to select a single cell instead of the whole row in QML.

![TableSelection](https://issues.cern.ch/secure/attachment/56690/TableViewSampleTabs2.gif)



## [TabTableViewAddRemove.qml](TabTableViewAddRemove.qml) <a name="TabTableViewAddRemove"></a>
[|Up|](#TOC) This [source file](TabTableViewAddRemove.qml) is containing a QML example to illustrate removing of a row in a [TableView](http://doc.qt.io/qt-5/qml-qtquick-controls-tableview.html)

:confused: It is not yet finished as I need to implement the row addition and neither to read the [`cities.json`](cities.json) file from QML without using C++ call yet.


![Add remove rows](https://issues.cern.ch/secure/attachment/56744/DeleteRow1.gif)
