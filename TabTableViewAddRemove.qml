import QtQuick 2.11
import QtQuick.Controls 1.4

Item {

   TableView {
      id: tableView
      anchors.fill: parent

      //      Component.onCompleted: {
      //         var request = new XMLHttpRequest()
      //         request.open("GET", "./cities.json", false)
      //         request.send(null)
      //         console.log(request.responseText)
      //         var my_JSON_object = JSON.parse(request.responseText)
      //         view.model = my_JSON_object
      //         //  model: JSON.parse("cities.json")
      //      }
      // model: JSON.parse("cities.json")
      // model: "cities.json"
      model: [{
            "area": "1928",
            "city": "Shanghai",
            "country": "China",
            "flag": "22px-Flag_of_the_People's_Republic_of_China.svg.png",
            "population": "13831900"
         }, {
            "area": "603",
            "city": "Mumbai",
            "country": "India",
            "flag": "22px-Flag_of_India.svg.png",
            "population": "13830884"
         }, {
            "area": "3527",
            "city": "Karachi",
            "country": "Pakistan",
            "flag": "22px-Flag_of_Pakistan.svg.png",
            "population": "12991000"
         }, {
            "area": "431.09",
            "city": "Delhi",
            "country": "India",
            "flag": "22px-Flag_of_India.svg.png",
            "population": "12565901"
         }]

      TableViewColumn {
         role: 'city'
         title: "City"
      }
      TableViewColumn {
         role: 'country'
         title: "Country"
         width: 120
      }
      TableViewColumn {
         role: 'area'
         title: "Area"
         width: 100
      }
      TableViewColumn {
         role: 'population'
         title: "Population"
         width: 100
      }
      TableViewColumn {
         delegate: Item {
            Image {
               anchors.centerIn: parent
               source: 'images/flags/' + styleData.value
            }
         }
         role: 'flag'
         title: "Flag"
         width: 60
      }
      TableViewColumn {
         role: 'delete'
         title: "Delete row"
         delegate: Button {
            iconSource: "images/remove.png"
            onClicked: {
               var data = tableView.model
               data.splice(styleData.row, 1)
               tableView.model = data
            }
         }
         width: 60
      }
   }
}
