import QtQuick 2.4
import QtQuick.Controls 1.4 as QT14
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Item {
   id: item1
   readonly property color selectedTabBorderColor: "#80dfff"
   readonly property color selectedTabColor: "#777777"
   readonly property color unselectedTabColor: "#CCCCCC"
   property alias currentIndex: tabBar.currentIndex

   TabBar {
      id: tabBar
      anchors.top: parent.top
      anchors.topMargin: 0
      anchors.left: parent.left
      anchors.right: parent.right
      background: Rectangle {
         color: "#EEEEEE"
      }
      TabButton {
         text: qsTr("Sort & Filter")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 0 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 0 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
      TabButton {
         text: qsTr("Button renderer")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 1 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 1 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
      TabButton {
         text: qsTr("Image Renderer")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 2 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 2 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
      TabButton {
         text: qsTr("Row and Item delegate")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 3 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 3 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
      TabButton {
         text: qsTr("Add / Remove")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 4 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 4 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
      TabButton {
         text: qsTr("Scrolling table")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 5 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 5 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
   }

   StackLayout {
      // anchors.fill.width: parent.width
      anchors.top: tabBar.bottom
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.topMargin: 0
      currentIndex: tabBar.currentIndex

      TabTableViewSort {
         id: tableViewSortTab
      }

      TabTableViewRendererButton {
         id: tableViewRenderers
      }

      TabTableViewRendererImage {
         id: tableViewImages
      }

      TabTableViewRowAndItemDelegate {
         id: tableViewDelegates
      }

      TabTableViewAddRemove {
         id: tabTableViewAddRemove
      }
      TabTableViewScroll {
         id: tabTableViewScroll
      }
   }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
