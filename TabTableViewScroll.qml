import QtQuick 2.11
import QtQuick.Controls 1.4

Item {
   //////////////////////////////////////////////////////////////////////
   // @TODO: remove this code START
   // when we will adapt this code to display real data
   property int maxSuperCycleBasicPeriods: 22
   property variant particleTypes: ["-", "PROTON", "NOBEAM", "PB54", "AR11", "XE39", "PB80"]
   property variant destinations: ["PS_DUMP", "TT2_D3", "TT2_FTA", "HIRADMAT", "FREE_5", "EAST_T8", "EAST_N", "FREE_8", "FREE_9", "NTOF", "LHC", "SPS_DUMP", "FREE_13", "FREE_14", "FREE_15", "AWAKE", "FTARGET", "FREE_18"]
   property variant lsaCycles: ["AD", "EAST_Irrad", "EAST_North", "LHC_ION_Early_PB54_H16H21", "ILHC100#4b", "MD4323_IonLifetime_magn_his", "LHC25#12b", "LHC25#12b_BCMS", "LHC25#72b", "LHC25#48b_BCMS_PS_TFB_2018", "LHCINDIV_PS", "LHCPROBE_PS", "TOF_RotationCheck", "MD3367_LHC3_H8", "MD4263_LHC25#48b_BCMS_PS_TFB_1BP_2018", "MD3391_LHCINDIV", "MD3223_LHC25#48b_BCMS_PS", "MD3763_LHC25#12b_BCMS", "MD3464_LHC25#72b", "MD_LHC25#12b_BCMS_For80b_2018", "MTE_2018_", "MTE_2018_LowInt", "TOF_doubleFB", "~~zero~~"]

   Timer {
      id: scrollTimer
      interval: 1200
      running: true
      repeat: true
      property int rowIndex: 0

      onTriggered: {
         // update table model here
         rowIndex = rowIndex + getRandomInt(1, 3)
         if (rowIndex > maxSuperCycleBasicPeriods)
            rowIndex = 1
         updateTable(rowIndex)
      }
   }

   Keys.onPressed: {
      if (event.key === Qt.Key_Space)
         scrollTimer.running = !scrollTimer.running
      console.log(scrollTimer.running)
   }

   function getRandomInt(minValue, maxValue) {
      minValue = Math.ceil(minValue)
      maxValue = Math.floor(maxValue)
      var returnedvalue = Math.floor(
               Math.random() * (maxValue - minValue + 1)) + minValue
      //console.log(returnedvalue)
      return returnedvalue
   }
   // @TODO: remove this code END
   //////////////////////////////////////////////////////////////////////

   // Global variables definition
   property variant colors: ["#FFFFFF", "#00FF00", "#FFBF00", "#FF0000"]
   property string nextLsaCycle: ""
   property string nextCycleIndex: ""
   property string nextParticle: ""
   property string nextDestination: ""

   function updateTable(rowIndex) {
      // remove the previously displayed "NEXT" row if any
      if (scrollingTableModel.count >= 2) {
         scrollingTableModel.remove(scrollingTableModel.count - 1)
         // Append row data for CURRENT cycle (coem of them were recieved previosusly and saved in the nextXxx variables)
         // @TODO the color must be evaluated from the received hardware values
         scrollingTableModel.append({
                                       "rowColor": rowIndex === 1 ? "red" : "black",
                                                                    "bpn": "" + rowIndex,
                                                                    "lsaCycle": nextLsaCycle,
                                                                    "lsaCycleColor": colors[getRandomInt(0, colors.length - 1)],
                                                                    "cycleIndex": nextCycleIndex,
                                                                    "intensity": (rowIndex * Math.random()).toFixed(
                                                                       2),
                                                                    "particle": nextParticle,
                                                                    "particleColor": colors[getRandomInt(0, colors.length - 1)],
                                                                    "destination": nextDestination,
                                                                    "destinationColor": colors[getRandomInt(0, colors.length - 1)]
                                    })
      } else {
         // Append one row of EMPTY data when the appllication is starting
         scrollingTableModel.append({
                                       "rowColor": rowIndex === 1 ? "red" : "black",
                                                                    "bpn": "",
                                                                    "lsaCycle": "",
                                                                    "lsaCycleColor": "black",
                                                                    "cycleIndex": "",
                                                                    "intensity": "",
                                                                    "particle": "",
                                                                    "particleColor": "black",
                                                                    "destination": "",
                                                                    "destinationColor": "black"
                                    })
      }

      // @TODO Replace dummy values by real hardware ones when available
      // Keep  the NEXT recieved values to be used as current valeus for the next time this method is called
      nextLsaCycle = lsaCycles[getRandomInt(0, lsaCycles.length - 1)].substring(
               0, scrollingTableModel.maxLsaCycleLength)
      nextCycleIndex = getRandomInt(1, 24)
      nextParticle = particleTypes[getRandomInt(0, particleTypes.length - 1)]
      nextDestination = destinations[getRandomInt(0, destinations.length - 1)]

      // Append NEXT cycle data
      scrollingTableModel.append({
                                    "rowColor": "black",
                                    "bpn": "/" + maxSuperCycleBasicPeriods,
                                    "lsaCycle": nextLsaCycle,
                                    "lsaCycleColor": "white",
                                    "cycleIndex": nextCycleIndex,
                                    "intensity": "",
                                    "particle": nextParticle,
                                    "particleColor": "white",
                                    "destination": nextDestination,
                                    "destinationColor": "white"
                                 })

      // remove first table row in order to make the table scrolling
      if (scrollingTableModel.count >= scrollingTableModel.maxLines) {
         scrollingTableModel.remove(0)
      }

      // Uncomment the following console.log on debug mode only:
      // Dump last inserted value in the table list model
      //      console.log("scrollingTableModel Dump=" + JSON.stringify(
      //                     scrollingTableModel.get(scrollingTableModel.count - 1)))
   }

   ListModel {
      id: scrollingTableModel
      readonly property int maxLines: 11
      readonly property int maxLsaCycleLength: 12
   }

   TableView {
      id: tableView
      anchors.fill: parent

      model: scrollingTableModel

      // hides the table header
      headerVisible: false
      readonly property int rowHeight: 20

      // This row delegate allow rows of a specific height and black background color.
      // This rowDelegate Component allows also the display of an horizontal line in the table:
      // - red when the cycle index is 1
      // - white to separate the last two lines displaying the curent and NEXT data
      rowDelegate: Component {
         id: rowDelegate
         Item {
            // The "line" separator is artificially created by supperposing two rectangles...
            Rectangle {
               width: tableView.width
               color: {
                  // dont know how the row could be -1 here but this happens sometime ???
                  if (styleData.row !== -1) {
                     if (styleData.row === (scrollingTableModel.count - 2))
                        return "white"
                     var listElement = scrollingTableModel.get(styleData.row)
                     if (listElement) {
                        return listElement["rowColor"]
                     }
                  }
                  return "gray"
               }
               height: tableView.rowHeight
            }
            // ...with a vertical decay of one pixel.
            Rectangle {
               width: tableView.width
               y: 1
               color: "black"
               height: tableView.rowHeight - 1
            }
         }
      }

      //////////////////////////////////////////////////////////////////////
      // TableViewColumn definition START
      TableViewColumn {
         role: 'bpn'
         title: "Bpn"
         width: 30
         delegate: Text {
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            color: "white"
            text: styleData.value
         }
      }
      TableViewColumn {
         role: 'lsaCycle'
         title: "LSA cycle"
         width: 120
         delegate: Text {
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            color: {
               if (styleData.row !== -1) {
                  var listElement = scrollingTableModel.get(styleData.row)
                  if (listElement) {
                     return listElement["lsaCycleColor"]
                  }
               }
               return "gray"
            }
            text: styleData.value
         }
      }
      TableViewColumn {
         role: 'cycleIndex'
         title: "Cycle index"
         width: 30
         delegate: Text {
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            color: {
               if (styleData.row !== -1) {
                  var listElement = scrollingTableModel.get(styleData.row)
                  if (listElement)
                     return listElement["lsaCycleColor"]
               }
               return "gray"
            }
            text: styleData.value
         }
      }
      TableViewColumn {
         role: 'intensity'
         title: "Intensity"
         width: 80
         delegate: Text {
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            color: "white"
            text: styleData.value
         }
      }
      TableViewColumn {
         role: 'particle'
         title: "Particle Type"
         width: 60
         delegate: Text {
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            color: {
               if (styleData.row !== -1) {
                  var listElement = scrollingTableModel.get(styleData.row)
                  if (listElement)
                     return listElement["particleColor"]
               }
               return "gray"
            }
            text: styleData.value
         }
      }
      TableViewColumn {
         role: 'destination'
         title: "Destination"
         width: 80
         delegate: Text {
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            color: {
               if (styleData.row !== -1) {
                  var listElement = scrollingTableModel.get(styleData.row)
                  if (listElement)
                     return listElement["destinationColor"]
               }
               return "gray"
            }
            text: styleData.value
         }
      }
      // TableViewColumn definition END
      //////////////////////////////////////////////////////////////////////
   }
}
